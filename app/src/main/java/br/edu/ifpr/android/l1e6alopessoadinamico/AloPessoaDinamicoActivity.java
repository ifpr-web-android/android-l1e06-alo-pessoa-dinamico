package br.edu.ifpr.android.l1e6alopessoadinamico;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class AloPessoaDinamicoActivity extends AppCompatActivity {

    public static final String TAG = "AloPessoaDinamActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_pessoa_dinamico);

        EditText editTextPessoa = (EditText) findViewById(R.id.editText_pessoa);

        editTextPessoa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                TextView alo_pessoa = (TextView) findViewById(R.id.alo_pessoa);
                alo_pessoa.setText(cumprimenta(editable.toString()));

            }
        });


    }

    private String cumprimenta(String pessoa){
        if("".equals(pessoa)){
            return "";
        }
        return "Alô, " + pessoa;
    }
}
